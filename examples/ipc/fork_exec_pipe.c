#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <string.h>

int pid;
int pc[2]; /* Parent to child pipe */
int cp[2]; /* Child to parent pipe */

int child_run_command() {
  char *args[] = { "/usr/bin/file", "-n", "-f", "-"};

  fprintf(stderr, "In child before exec\n");
  execv(args[0], args);
  perror("No exec");
  kill(getppid(), SIGQUIT);
  return 1;
}

int parent_do_work() {
  
  char *file_list[] = {"/bin/ls\n", "/etc/fstab\n", "/usr/bin/file\n", NULL};
  char *file_name = NULL;
  int i = 0;
  char *buff = NULL;
  int buff_size = 16*1024;
  int res;

  file_name = file_list[i];

  // Sending the files to the /usr/bin/file command .... 
  while(file_list[i]) {
    printf("Parent sending file name %s\n", file_list[i]);
    write(pc[1], file_list[i], strlen(file_list[i]));
    i++;
  }

  // Reading the results 
  buff = malloc(buff_size);
  sleep(1);
  printf("Parent reading from child pipe\n");

  res = read(cp[0], buff, buff_size);

  printf("Got from child [%s]\n", buff);
  //close(pc[1]);
  //close(cp[1]);
  return 1;
}

int main() {
  
  /* Make pipes */
  if( pipe(pc) < 0)
    {
      perror("Can't make pipe");
      exit(1);
    }
  if( pipe(cp) < 0)
    {
      perror("Can't make pipe");
      exit(1);
    }
  
  
  /* Create a child to run command. */
  switch( pid = fork() )
    {
    case -1: 
      perror("Can't fork");
      exit(1);

    case 0:
      /* Child. */
      printf("Child preparing pipe side\n");
      close( cp[0]);
      dup2( cp[1], STDOUT_FILENO); /* Make stdout go to write
		      end of pipe. */


      dup2( pc[0], STDIN_FILENO); /* Make stdin come from read
		      end of pipe. */
      close( pc[1]);

      child_run_command();
      exit(1);

    default:
      /* Parent. */
      /* Close what we don't need. */
      parent_do_work();
      exit(0);
    }
  
}
