#include<stdio.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<string.h>

int main()
{
  int fd,fname;
  mkfifo("/tmp/fifo1",0600);
  fd=open("/tmp/fifo1",O_RDONLY);
  int d = 0;
  while(read(fd,&d,sizeof(int))!=0)
    {
      printf("%d\n",d);
      usleep(10);
    }
  close(fd);
  return 1;
}
