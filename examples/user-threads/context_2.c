#include <malloc.h>
#include <ucontext.h>
#include <stdio.h>
#include <stdlib.h>

// 64kB stack
#define FIBER_STACK 1024*64

ucontext_t child_1 , child_2, parent;

// The child thread will execute this function
void threadFunction_1()
{
  printf( "Child 1 fiber yielding to parent\n" );
  swapcontext( &child_1, &parent );
  printf( "Child 1 thread exiting\n" );
  swapcontext( &child_1, &parent );
}

void threadFunction_2()
{
  printf( "Child 2 fiber yielding to parent\n" );
  swapcontext( &child_2, &parent );
  printf( "Child 2 thread exiting\n" );
  swapcontext( &child_2, &parent );
}

int main()
{
  // Get the current execution context
  getcontext( &child_1 );
  
  // Modify the context to a new stack
  child_1.uc_link = 0;
  child_1.uc_stack.ss_sp = malloc( FIBER_STACK );
  child_1.uc_stack.ss_size = FIBER_STACK;
  child_1.uc_stack.ss_flags = 0;        
  if ( child_1.uc_stack.ss_sp == 0 )
    {
      perror( "malloc: Could not allocate stack" );
      exit( 1 );
    }
  
  // Create the new context
  printf( "Creating child 1 fiber\n" );
  makecontext( &child_1, &threadFunction_1, 0 );

  // Get the current execution context
  getcontext( &child_2 );
  
  // Modify the context to a new stack
  child_2.uc_link = 0;
  child_2.uc_stack.ss_sp = malloc( FIBER_STACK );
  child_2.uc_stack.ss_size = FIBER_STACK;
  child_2.uc_stack.ss_flags = 0;        
  if ( child_2.uc_stack.ss_sp == 0 )
    {
      perror( "malloc: Could not allocate stack" );
      exit( 1 );
    }
  
  // Create the new context
  printf( "Creating child fiber\n" );
  makecontext( &child_2, &threadFunction_2, 0 );



  
  // Execute the child context
  printf( "Switching to child 1 fiber\n" );
  swapcontext( &parent, &child_1 );

  printf( "Switching to child 2 fiber\n" );
  swapcontext( &parent, &child_2 );


  printf( "Switching to child 1 fiber again\n" );
  swapcontext( &parent, &child_1 );

  printf( "Switching to child 2 fiber again\n" );
  swapcontext( &parent, &child_2 );
  
  // Free the stack
  free( child_1.uc_stack.ss_sp );
  free( child_2.uc_stack.ss_sp );
  
  printf( "Child fibers returned and stack freed\n" );
  
  return 0;
}
