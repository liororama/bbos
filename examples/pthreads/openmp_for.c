#include <omp.h>
#include <stdlib.h>
#include <stdio.h>

#define CHUNKSIZE 100
#define N     100000000

main ()  
{

  int i, chunk;
  int j;
  float *a, *b, *c;
  double t_start, t_end;
  int current_num_threads;
  a = (float *) malloc(sizeof(float) * N);
  b = (float *) malloc(sizeof(float) * N);
  c = (float *) malloc(sizeof(float) * N);

  /* Some initializations */
  for (i=0; i < N; i++)
    a[i] = b[i] = i * 1.0;
  
  current_num_threads = omp_get_num_threads();
  printf("T: %d\n", current_num_threads);

  chunk = CHUNKSIZE;
  t_start = omp_get_wtime();
#pragma omp parallel shared(a,b,c,chunk) private(i,j)
  {
    
#pragma omp for 
    for (i=0; i < N-140; i++) {
      for(j=0 ; j < 3 ; j++) {
	c[i] = a[i] + b[i];
	c[i] = c[i] + 3;
	c[i] = 3*a[i+j] + 3;
	c[i] = 3*c[i+j] + b[+j];
      }
    }

  }  /* end of parallel section */


  t_end = omp_get_wtime();
  printf("Total loop time %.6f\n", t_end - t_start);
}
