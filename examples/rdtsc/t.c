#include <stdio.h>
#include <stdlib.h>

static __inline__ unsigned long long rdtsc(void)
{
  unsigned hi, lo;
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

int main(int argc, char* argv[])
{
  unsigned long long a,b;
  int i;
  int j;
  int *int_arr;
  int s = 1000000;
  int_arr = (int *) malloc(sizeof(int) * s);
  
  a = rdtsc();
  
  //for(i=0; i< s ; i++){
    //int_arr[i] = 5;
  //  j = i + 1;
  // }
  
  b = rdtsc();

  printf("%llu\n", b-a);
  return 0;
}


