#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>

int main(int argc, char *argv[])
{
   uid_t uid;
   uid = syscall(SYS_getuid);
   printf("uid=%d\n",uid);
}
